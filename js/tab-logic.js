function initTabLogic () {
  $$('#map').on('show', function () {
    console.log('show');
    $$('#map-filter').css('visibility', 'visible');
    $$('#search-map').css('visibility', 'visible');
  });

  $$('#achievements, #info, #scores').on('show', function () {
    console.log('hide');
    $$('#map-filter').css('visibility', 'hidden');
    $$('#search-map').css('visibility', 'hidden');
  });
}
