
//map
contentString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit."

var directionsDisplay = new google.maps.DirectionsRenderer(
  {
      suppressMarkers: true
  });
var directionsService = new google.maps.DirectionsService;

var spots = {};
spots["green"] = [
  {position: {lat: 46.066019, lng: 14.5465247}, title: "BUS stop", label: "BUS stop", description: "LPP station." , eventFlag: "simple"},
  {position: {lat: 46.062115, lng: 14.5438587}, title: "CITYBus", label: "CITYBus", description: "LPP station." , eventFlag: "simple"},
  {position: {lat: 46.065297, lng: 14.5428497}, title: "BicikeLJ", label: "BicikeLJ", description: "Self service bike offering.", eventFlag: "simple"},
  {position: {lat: 46.039964, lng: 14.471359}, title: "Halcom", label: "Halcom", description: "IT firm.", eventFlag: "simple"}
];
spots["drinks"] = [
  {position: {lat: 46.064854, lng: 14.5459608}, title: "Zvezda", label: "Kavarna Zvezda", description: "A bar." , eventFlag: "simple"},
  {position: {lat: 46.06333, lng: 14.5445078}, title: "Hood burger", label: "Hood burger", description: "A gourmet fast food restaurant." , eventFlag: "mBills"},
  {position: {lat: 46.069495, lng: 14.5468358}, title: "Mcdonalds", label: "Mcdonalds", description: "A fast food restaurant", eventFlag: "false"}
];
spots["health"] = [
  {position: {lat: 46.068953, lng: 14.5432798}, title: "FITINN", label: "FITINN Fitnessstudio", description: "Fitness service.", eventFlag: "simple"},
  {position: {lat: 46.067693, lng: 14.5416088}, title: "Fitness shop BTC", label: "Fitness shop BTC", description: "Fitness service." , eventFlag: "mBills"},
  {position: {lat: 46.063314, lng: 14.547998}, title: "Atlantis", label: "Atlantis", description: "Water park." , eventFlag: "mBills"}
];
spots["electronic"] = [
  {position: {lat: 46.066347, lng: 14.5436888}, title: "Mimovrste", label: "Mimovrste", description: "Electronics store." , eventFlag: "mBills"},
  {position: {lat: 46.063818, lng: 14.5442037}, title: "Big Bang", label: "Big Bang", description: "Electronics store." , eventFlag: "mBills"},
  {position: {lat: 46.064527, lng: 14.5450218}, title: "Mlacom d.o.o.", label: "Mlacom d.o.o.", description: "Electronics store." , eventFlag: "mBills"}
];

var pins = {green: "img/custom/GreenPin.svg", health: "img/custom/run.svg", electronic: "img/custom/shop.svg", drinks: "img/custom/forkSpoon.svg"};

var stores = {atlantis: {lat: 46.063027, lng: 14.548655}};

infowindow = new google.maps.InfoWindow();
distanceThreshold = 30 // in meters

var markers = {};
markers["drinks"] = [];
markers["green"] = [];
markers["electronic"] = [];
markers["health"] = [];

function getCategoryIcon(category){
  return pins[category];
}


function pinClean(category) {
  for (var i = 0; i < markers[category].length; i++) {
    markers[category][i].setMap(null);
  }
  markers[category] = [];
}

function singlePin(pin, category) {
  infowindow = new google.maps.InfoWindow({
    content: pin.label
  });

  //marker, with
  var marker = new google.maps.Marker({
    position: pin.position,
    map: map,
    title: pin.title,
    icon: getCategoryIcon(category)
  });

  marker.addListener('click', function() {
    infowindow.close();
    infowindow.setContent('<p class="map-pin-title">' + pin.label + '</p> <span class="map-pin-subtitle">' + pin.description + '</span>' +
     '<p class="button-more-info-container"><button class="button-more-info color-main" onclick="openGreenOfferPopup()"><span>More info</span></button></p>');
    infowindow.open(map, marker);
  });
  return marker;
}

function pinSet(category) {
  for (var i = 0; i < spots[category].length; i++){
    markers[category].push(singlePin(spots[category][i], category));
  }
}

var userPosition = null;

function initializeMap() {
  var mapProp = {
    center:new google.maps.LatLng(46.0662855,14.5449612),
    zoom:16,
    disableDefaultUI:true,
    zoomControl:true,
    scaleControl:true,
    mapTypeId:google.maps.MapTypeId.ROADMAP,
    styles: [
      {
        featureType: "poi.business",
        elementType: "labels",
        stylers: [
          { visibility: "off" }
        ]
      }
    ]
  };

  map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

  //var opt = { minZoom:16, maxZoom: 20 };
  //map.setOptions(opt);

  var strictBounds = new google.maps.LatLngBounds(
    new google.maps.LatLng(46.0601172, 14.5390192),
    new google.maps.LatLng(46.070029, 14.5473233)
  );

  //Premaknjena mapa, se sproži event handler
  google.maps.event.addListener(map, 'dragend', function() {
    if (strictBounds.contains(map.getCenter())) return;

    // Preveri če smo znotraj območja, nastavi na rob drugače
    var c = map.getCenter(),
      x = c.lng(),
      y = c.lat(),
      maxX = strictBounds.getNorthEast().lng(),
      maxY = strictBounds.getNorthEast().lat(),
      minX = strictBounds.getSouthWest().lng(),
      minY = strictBounds.getSouthWest().lat();

    if (x < minX) x = minX;
    if (x > maxX) x = maxX;
    if (y < minY) y = minY;
    if (y > maxY) y = maxY;

    //map.setCenter(new google.maps.LatLng(y, x));
  });

  google.maps.event.addListenerOnce(map, 'idle', function(){
    callBackLocation();
  });
}

function callBackLocation(){
  getLocation();
  //setTimeout(callBackLocation, 2000);
}

function getLocation(){
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
    pinSet("green");
  }
  else{
    //show that it is not available, try again
  }
}

function showPosition(position) {

  userPosition = {lat: position.coords.latitude, lng: position.coords.longitude};
  // hardcoded location for demonstration since we are not in BTC
  userPosition = {lat: 46.065184, lng: 14.542916};
  var user_marker = new google.maps.Marker({
    position: userPosition,
    map: map,
    icon: "img/custom/markerUser.svg"
  });
  markerDetected = detect(userPosition);


  if (markerDetected.eventFlag == "simple") {
    points+= 10;
    refreshPoints();
    setTimeout(function () {
      myApp.addNotification({
          title: 'You reached a green point: BicikeLJ!',
          subtitle: 'You gained 10 green points.',
          message: 'Tap on this notification to earn more points!',
          media: '<img width="44" height="44" style="border-radius:100%" src="img/custom/coins.svg">',
          closeOnClick: true,
          onClick: function() {
            myApp.popup('.popup-green-spot');
          }
      });
    }, 20000);

  } else if (markerDetected.eventFlag == "mBills") {
    points+= 10;
    refreshPoints();
    myApp.addNotification({
        title: 'You reached a green offer: Atlantis!',
        subtitle: 'You gained 10 green points.',
        message: 'Tap on this notification to earn more points!',
        media: '<img width="44" height="44" style="border-radius:100%" src="img/custom/coins.svg">',
        closeOnClick: true,
        onClick: openGreenOfferPopup
    });
 }
}

function detect(userPosition) {
  for (key in spots) {
    for (var i = 0; i < spots[key].length; ++i) {
      var a = new google.maps.LatLng(spots[key][i].position.lat, spots[key][i].position.lng);
      var b = new google.maps.LatLng(userPosition.lat, userPosition.lng);
      var distance = google.maps.geometry.spherical.computeDistanceBetween(a,b);
      if (distance <= distanceThreshold) {

        return spots[key][i];
      }
    }
  }
}

function pathToStore(endPosition) {
 directionsService.route({
   origin: userPosition,
   destination: stores[endPosition],
   travelMode: google.maps.TravelMode.WALKING
 }, function(response, status) {
   if (status === google.maps.DirectionsStatus.OK) {
     directionsDisplay.setMap(map);
     directionsDisplay.setDirections(response);
   }
 });
}

function search(){
 pathToStore("atlantis");
}

function openGreenOfferPopup (){
  myApp.popup('.popup-green-offer');
}

google.maps.event.addDomListener(window, 'load', initializeMap);
