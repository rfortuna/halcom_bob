
var myApp;
var $$;
var points = 156;

//popup - categories
window.onload = function (){
 myApp = new Framework7();
 $$ = Dom7;
 var mainView = myApp.addView('.view-main', {
     // Because we use fixed-through navbar we can enable dynamic navbar
     dynamicNavbar: true
 });

 myApp.onPageInit('index', function (page) {
  console.log('index initialized');
  initializeMap();
});

//handling menu highliights
myApp.onPageInit('*', function (page) {
  // $$(".item-link").removeClass("item-content-active")
  // $$(".menu-" + page.name).addClass("item-content-active");
});

//load partials
$(".popup-select-categories").load("html/popup-select-categories.html");
$(".popup-green-spot").load("html/popup-green-spot.html");
$(".popup-green-offer").load("html/popup-green-offer.html");
$("#achievements").load("html/achievement.html");
$("#scores").load("html/scores.html");
$("#info").load("html/info.html");

//setup flipping animation
$$('body').on('click', '.flip', function (){
  $$(this).find('.flip-card').toggleClass('flipped');

  points+= 100;
  //trigger achievement popup-close
  setTimeout(
    function() {
      myApp.addNotification({
        title: 'New achievement: The Bike Master!',
        message: '<div style="margin-top: 10px;">Share it to get extra points: <i class="icon ion-android-share-alt icon-share"></i></div>',
        media: '<img style="width: 60px;" src="img/custom/Coins-01.svg" />'
    });
    }, 200
  )
  refreshPoints();
});

//the pin switching logic
$$('body').on("click","#close" , function(){
  pinClean("drinks");
  pinClean("electronic");
  pinClean("health");

  if($("#health").is(':checked')){ pinSet("health")};
  if($("#electric").is(':checked')){ pinSet("electronic")};
  if($("#drink").is(':checked')){ pinSet("drinks")};
});

initTabLogic();
refreshPoints();
}

function refreshPoints() {
 $$("#points-container").text(points);
}
