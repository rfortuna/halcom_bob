function hall_button(){
 var request = new XMLHttpRequest();

   request.open('POST', 'https://demo3.halcom.com/MBillsWS/API/v1/transaction/sale');
   request.setRequestHeader('Content-Type','application/json');
   request.setRequestHeader('Authorization','Basic MTAwMDAwMDEuMS4xNDYyMjcxNzcxOjEyMzQ1Ng');
   request.setRequestHeader('Access-Control-Allow-Origin', '*');
   request.setRequestHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
   request.setRequestHeader('Access-Control-Allow-Headers', 'Content-Type, Content-Range, Content-Disposition, Content-Description');

   request.onreadystatechange = function() {
       if(this.readyState === 4){
           //intent
           var response = JSON.parse(this.responseText);
           alert(response.paymenttokennumber);

           finishSale(response.paymenttokennumber);
       }

   };


   var body = {
       'amount': 1000,
       'currency':'EUR',
       'purpose':'Online store',
       'paymentreference':'SI00',
       'orderid':'12334',
       'channelid':'green'
   };

   console.log(JSON.stringify(body));
   request.send(JSON.stringify(body));
}

function finishSale(token){
       alert('mbillsdemo://www.mbills.si/dl/?type=1&token='+token);
       window.open('mbillsdemo://www.mbills.si/dl/?type=1&token='+token, '_system');
}
